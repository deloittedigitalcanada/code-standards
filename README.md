# Deloitte Digital Code Standards

Information of this guide is a compilation of information from different sources. Much of the guidance in this document leans on open source designs, code, and patterns from corporate, civic and government organizations. Complete list of references at the end of the document.

## About

This repo is meant to be a companion for every project you develop. This repo and some of the boilerplate files (.gitignore, .eslint, etc) need to be pulled in into your project. More instructions below.

### Get this repo as a submodule of your project for continuous reference while developing

As a main rule, this repo should be your first stop regarding any code standards. Please consult, read and collaborate to this repo by opening tickets [here](https://bitbucket.org/deloittedigitalcanada/code-standards/issues). Form more information about Git Submodules, visit this [link](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

1. Add the repo as a submodule by running

```bash
git submodule add git@bitbucket.org:deloittedigitalcanada/code-standards.git
```

2. Add `.gitignore`
3. Add `.eslint`
4. Add `.nvmrc` and lock your npm version

## Sources

We have selected and compiled most of these rules from different resources. The ones that you will find here are the most important to follow. More details can be find in the links below. Feel free to consult these resources at any time.

**Please report broken resources**.

- [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html)
- [HTML5 Style Guide and Coding Conventions](https://www.w3schools.com/html/html5_syntax.asp)
- [Airbnb CSS / Sass Style Guide](https://github.com/airbnb/css)
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- [Markup best practices - 10up](https://10up.github.io/Engineering-Best-Practices/markup/)
- [Github's Primer CSS](https://primer.style/design/global/accessibility)
- [Polaris by Shopify](https://polaris.shopify.com/)
