# Accessibility

Accessibility is everyone’s responsibility, and the purpose of this document is to provide general accessibility guidelines to developers and designers.

It’s important that our clients and their customers are able to use the products that we create for them. Accessibility means creating a web that is accessible to all people: those with disabilities and those without. We must think about people with visual, auditory, physical, speech, cognitive and neurological disabilities and ensure that we deliver the best experience we possibly can to everyone. Accessibility best practices also make content more easily digestible by search engines. Increasingly, basic accessibility can even be a legal requirement. In all cases, an accessible web benefits everyone.

## Accessibility Standards

At a minimum, all projects should pass WCAG 2.1 Level A Standards. Level AA should be the baseline goal. Having direct access to the designer also allows for greater accessibility standards to be achieved.

Following the guidance of [Web Content Accessibility Guidelines 2.0](https://www.w3.org/TR/WCAG20/#conformance) at Level AA will help a project pass Section 508 as well and also maintain a consistent standards. If a project specifically requires Section 508, additional confirmation testing can be done.

Making products accessible benefits everyone, not just people with disabilities. Below are some examples of use cases in which accessibility is important:

- **Visual**: blindness, low vision, color blindness, using a screen reader or related assistive tech for lifestyle reasons (e.g. long car commute), machine readability and screen scraping technologies

- **Hearing**: deafness, hearing impairment, speech impairment, using closed captioning or other assistive features for lifestyle reasons (e.g. coworking in a loud coffee shop)

- **Cognitive**: including short-term memory issues, dyslexia, learning disabilities, trying to work or consume content while distracted or multitasking, etc.

- **Mobility**: mobility impairments, repetitive stress injuries, power users who love keyboard shortcuts, busy parents holding a sleeping child while trying to operate a computer with one hand, etc.

## General guidelines

### Semantic markup

As explain in the [Semantics](#semantics) section, always use semantic HTML elements, like `button`, `ul`, `nav`. Most modern browsers implement the accessibility features outlined in the specs for these elements; without them, elements will need additional [ARIA attributes and roles](http://www.w3.org/TR/wai-aria/) to be recognized by assistive technologies.

Elements like `h1`-`h6`, `nav`, `footer`, `header` have [meaningful roles](https://www.w3.org/TR/wai-aria/roles#role_definitions_header) assigned, so use them, and use them carefully. This can help assistive technologies read the page better and help users find information quicker.

Only use a `div` or a `span` to markup up content when there isn't another HTML element that would semantically be more appropriate, or when an element is needed exclusively for applying CSS styles or JS behaviors.

```html
<!-- Do: Button can be focused and tabbed to. -->
<button type="button" class="btn">Send</button>
```

```html
<!-- Don't: Button can't be focused and tabbed to. -->
<span class="btn">Send</span>
```

More on semantic markup:

- [Semantic Structure – WebAIM](http://webaim.org/techniques/semanticstructure/)

### Keyboard accessibility

Make sure elements can be reached via tabbing, and actions can be triggered with a keyboard. Using semantic markup is especially important in this case as it ensures that actionable elements are tabbable and triggerable without a mouse. Note that elements positioned off-screen are still tabbable, but those hidden with `display: none` or `visibility: hidden` are effectively removed from content since they are neither read by screen readers nor reachable by keyboard.

Tab order is determined by the order of elements in the DOM, and not by their visual positioning on the page after CSS is applied. CSS properties `float` and `order` for flex items are two common sources for disconnection between visual and DOM order.

The `tabindex` attribute should only be used when absolutely necessary.

- `tabindex=0/-1` makes an element focusable, while `tabindex=0` also includes the element in the normal tab order. In both cases, keyboard triggers of the element still require scripting, so where possible, use [interactive content](http://w3c.github.io/html/dom.html#kinds-of-content-interactive-content) instead.

- `tabindex=1+` takes an element to the very front of the default tab order. When it's applied, the element's visual positioning is no longer indicative of its tab order, and the only way to find out where an element is would be by tabbing through the page. Therefore, unless a page is carefully designed around elements with positive tabindex, it is very error-proned, and thus currently prohibited in github.com.

Finally, bear in mind that some assistive technologies have keyboard combinations of their own that will override the combination on the web page, so don't rely on keyboard shortcuts as the only alternative to mouse actions.

```html
<!-- Do: Tab order can be guessed. -->
<button type="button" class="btn">1</button>
<button type="button" class="btn">2</button>
<button type="button" class="btn">3</button>
```

```html
<!-- Don't: The second button's tab order is unexpected. -->
<button type="button" class="btn">1</button>
<button type="button" class="btn" tabindex="1">2</button>
<button type="button" class="btn">3</button>
```

More on keyboard accessibility:

- [Focus Order – Understanding WCAG 2.0](https://www.w3.org/TR/UNDERSTANDING-WCAG20/navigation-mechanisms-focus-order.html)
- [Time to revisit accesskey? by Léonie Watson](http://tink.uk/time-to-revisit-accesskey/)
- [Flexbox & the keyboard navigation disconnect by Léonie Watson](http://tink.uk/flexbox-the-keyboard-navigation-disconnect/)

### Visual accessibility

Be mindful when using small font size, thin font weight, low contrast colors in designs as it can severely affect usability.

Instead of relying solely on color to communicate information, always combine color with another factor, like shape or position change. This is important because some colors can be hard to tell apart due to color blindness or weak eyesight.

More on visual accessibility:

- [Use of Color – Understanding WCAG 2.0](https://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-without-color.html)
- [Contrast – Understanding WCAG 2.0](http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html)

### Text and labels

Make sure text-based alternative is always available when using icons, images, and graphs, and that the text by itself presents meaningful information.

```html
<!-- Do: Link text communicates destination. -->
<p>Find out more about <a href="#url">Our team members</a>.<br /></p>
```

```html
<!-- Don't: "Click here" is not meaningful. -->
<p>To find out more about Our team members, <a href="#url">click here</a>.</p>
```

Use `title` to add information on top of existing content.

```html
<a
  title="@octocat's repositories"
  href="https://github.com/octocat?tab=repositories"
  >octocat</a
>
```

Use `aria-label` when there is no text.

```html
<a href="https://help.github.com/"
  ><%= octicon("question", :"aria-label" => "Help") %></a
>
```

### Link responsibly

Navigating from a list of all the links on a given web page is very common for assistive technology users. We should make sure that the link text itself is meaningful and unique, and there should be as few duplicated references as possible.

More on link responsibly:

- [Link Purpose – Understanding WCAG 2.0](https://www.w3.org/TR/UNDERSTANDING-WCAG20/navigation-mechanisms-refs.html)

### Dynamic content

When using JavaScript to change the content on the page, always make sure that screen reader users are informed about the change. This can be done with `aria-live`, or focus management.

More on dynamic content:

- [ARIA Live Regions – MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Live_Regions)

### Focus management

Focus management is useful for informing users about updated content, and for making sure users land on the next useful section after performing an action. This means using scripts to change user’s currently focused element, and when focus changes, screen readers will read out change.

### Accessible forms

It is common for assistive technology users to jump straight to a form when using a website, so make sure most relevant information is in the form and is labelled properly. Labels and inputs should be associated with the `label[for]` and `input[id]`, and help texts should either be part of the label or be associated with `aria-describedby`.

```html
<!-- Do: Click "Email" label to focus on the input, and help text is read out by screen readers when focusing on the input. -->
<label for="test_input">Email</label><br />
<input
  id="test_input"
  aria-describedby="test_input_help_good"
  class="width-full my-1"
  type="email"
/>
<div id="test_input_help_good" class="f6">
  Please enter an email ending with @github.com.
</div>
```

```html
<!-- Don't: Input and label are not associated, and help text is not read out by screen reader when focusing on the input. -->
<label>Email</label><br />
<input type="email" class="width-full my-1" />
<div id="test_input_help_bad" class="f6">
  Please enter an email ending with @github.com.
</div>
```

## Development tools

### Linting

We currently run basic linting [against positive `tabindex`](https://github.com/github/github/blob/8546895623677abc70f61951642f32c16de231a1/test/fast/linting/accessible_tabindex_test.rb) and [anchor links with `href="#"`](https://github.com/github/github/blob/8546895623677abc70f61951642f32c16de231a1/test/rubocop/cop/rails/link_href.rb).

We do client side scanning of inaccessible elements in development environment. The inaccessible elements will be styled with red borders with an onclick alert and console warnings.

### External tools

- Check out [the Web Accessibility showcase on GitHub](https://github.com/showcases/web-accessibility). GitHubbers are free to add more projects to the showcase.
- [Color Contrast Analyser](https://www.paciellogroup.com/resources/contrastanalyser/): Check the legibility and contrast of your app using this app for Mac and Windows.
- [Color Oracle](http://colororacle.org/): Simulate color blindess using this app available for Mac, Windows, and Linux.

### Manual testing

#### Screen reader

To use VoiceOver, the built-in screen reader for Mac, just hit <kbd>⌘</kbd> + <kbd>F5</kbd>. This will start voice narration and display most of the spoken information in a text box. Use <kbd>tab</kbd> to navigate over all focusable elements in the page.

Use <kbd>alt</kbd> + <kbd>control</kbd> + <kbd>left/right</kbd> to navigate a web page. <kbd>alt</kbd> + <kbd>control</kbd> + <kbd>space</kbd> to click on an element. For more help with VoiceOver, check out the built-in tutorial in `System Preferences > Accessibility > VoiceOver > Open VoiceOver Training`.

Keep in mind that behaviors in different screen readers can differ when navigating the same web page; just like designing for different browsers, when it comes to accessibility, it is not just the implementation of the browsers that can be different, but also the ones of assistive softwares.

##### Screen reader's combinations

### Automated Testing

## Resources

- [Handling common accessibility problems](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Accessibility)
- [Small Tweaks That Can Make a Huge Impact on Your Website’s Accessibility](https://css-tricks.com/small-tweaks-can-make-huge-impact-websites-accessibility/)
- [How Our CSS Framework Helps Enforce Accessibility](https://www.ebayinc.com/stories/blogs/tech/how-our-css-framework-helps-enforce-accessibility/)
- [Writing CSS with Accessibility in Mind](https://medium.com/@matuzo/writing-css-with-accessibility-in-mind-8514a0007939)
- [https://css-tricks.com/why-how-and-when-to-use-semantic-html-and-aria/](https://css-tricks.com/why-how-and-when-to-use-semantic-html-and-aria/)
- [How to Fail at Accessibility](https://slack.engineering/how-to-fail-at-accessibility-99bdf3504f19)
- [Top 5 Questions Asked in Accessibility Trainings](https://www.deque.com/blog/top-5-questions-asked-in-accessibility-trainings/)
- [Web accessibility collection tools on github](https://github.com/collections/web-accessibility)

### React specific

- [Accessibility](https://reactjs.org/docs/accessibility.html)
- [Accessibility auditing with react-axe and eslint-plugin-jsx-a11y](https://web.dev/accessibility-auditing-react/)

## Sources

We have selected and compiled most of these rules from different resources. The ones that you will find here are the most important to follow. More details can be find in the links below. Feel free to consult these resources at any time.

**Please report broken resources**.

- [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html)
- [HTML5 Style Guide and Coding Conventions](https://www.w3schools.com/html/html5_syntax.asp)
- [Airbnb CSS / Sass Style Guide](https://github.com/airbnb/css)
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- [Markup best practices - 10up](https://10up.github.io/Engineering-Best-Practices/markup/)
- [Github's Primer CSS](https://primer.style/design/global/accessibility)
- [Polaris by Shopify](https://polaris.shopify.com/)
