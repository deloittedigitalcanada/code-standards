# HTML

## General approach

You will find yourself writing plain HTML often when building static sites. A good structure of your HTML will help us with accessibility. Writing semantic HTML is crucial when it comes to accessibility compliance. Here are some rules we expect you to follow when writing plain HTML or HTML inside React components.

## Use Correct Document Type

Always declare the document type as the first line in your document:

```html
<!DOCTYPE html>

<!--If you want consistency with lower case tags, you can use:-->
<!doctype html>
```

## Meta Data

The `<title>` element is required in HTML5. Make the title as meaningful as possible:

```html
<title>HTML5 Syntax and Coding Style</title>
```

To ensure proper interpretation and correct search engine indexing, both the language and the character encoding should be defined as early as possible in a document:

```html
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8" />
    <title>HTML5 Syntax and Coding Style</title>
  </head>
</html>
```

Metadata is used by browsers (how to display content), by search engines (keywords), and other web services.

Define the character set used:

```html
<meta charset="UTF-8">
```

Define a description of your web page:

```html
<meta name="description" content="Free Web tutorials">
```

Define keywords for search engines:

```html
<meta name="keywords" content="HTML, CSS, XML, JavaScript">
```

Define the author of a page (optional):

```html
<meta name="author" content="John Doe">
```

### Setting The Viewport

HTML5 introduced a method to let web designers take control over the viewport, through the `<meta>` tag.

The viewport is the user's visible area of a web page. It varies with the device, and will be smaller on a mobile phone than on a computer screen.

You should include the following `<meta>` viewport element in all your web pages:

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

A `<meta>` viewport element gives the browser instructions on how to control the page's dimensions and scaling.

The `width=device-width` part sets the width of the page to follow the screen-width of the device (which will vary depending on the device).

The `initial-scale=1.0` part sets the initial zoom level when the page is first loaded by the browser.

## Protocol

Use HTTPS for embedded resources where possible.

Always use HTTPS (https:) for images and other media files, style sheets, and scripts, unless the respective files are not available over HTTPS.

- HTML

```html
<!-- Not recommended: omits the protocol -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Not recommended: uses HTTP -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Recommended -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
```

- CSS/SCSS

```scss
/* Not recommended: omits the protocol */
@import '//fonts.googleapis.com/css?family=Open+Sans';

/* Not recommended: uses HTTP */
@import 'http://fonts.googleapis.com/css?family=Open+Sans';

/* Recommended */
@import 'https://fonts.googleapis.com/css?family=Open+Sans';
```

## Delay JavaScript Loading

Putting your scripts at the bottom of the page body lets the browser load the page first.

While a script is downloading, the browser will not start any other downloads. In addition all parsing and rendering activity might be blocked.

> The HTTP specification defines that browsers should not download more than two components in parallel.

An alternative is to use `defer="true"` in the script tag. The defer attribute specifies that the script should be executed after the page has finished parsing, but it only works for external scripts.

## Indentation

Indent by 2 spaces at a time.

Don’t use tabs or mix tabs and spaces for indentation.

```html
<ul>
  <li>Fantastic</li>
  <li>Great</li>
</ul>
```

```scss
.example {
  color: blue;
}
```

## Capitalization

Use only lowercase.

All code has to be lowercase: This applies to HTML element names, attributes, attribute values (unless text/CDATA), CSS selectors, properties, and property values (with the exception of strings).

```html
<!-- Not recommended -->
<A HREF="/">Home</A>

<!-- Recommended -->
<img src="google.png" alt="Google" />
```

```css
/* Not recommended */
color: #E5E5E5;
/* Recommended */
color: #e5e5e5;
```

## Trailing Whitespace

Remove trailing white spaces.

Trailing white spaces are unnecessary and can complicate diffs.

```html
<!-- Not recommended -->
<p>What?</p>_

<!-- Recommended -->
<p>Yes please.</p>
```

## General Formatting

Use a new line for every block, list, or table element, and indent every such child element.

Independent of the styling of an element (as CSS allows elements to assume a different role per display property), put every block, list, or table element on a new line.

Also, indent them if they are child elements of a block, list, or table element.

```html
<blockquote>
  <p><em>Space</em>, the final frontier.</p>
</blockquote>

<ul>
  <li>Moe</li>
  <li>Larry</li>
  <li>Curly</li>
</ul>

<table>
  <thead>
    <tr>
      <th scope="col">Income</th>
      <th scope="col">Taxes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>$ 5.00</td>
      <td>$ 4.50</td>
    </tr>
  </tbody>
</table>
```

## HTML Line-Wrapping

Break long lines (optional).

While there is no column limit recommendation for HTML, you may consider wrapping long lines if it significantly improves readability.

When line-wrapping, each continuation line should be indented at least 4 additional spaces from the original line.

```html
<md-progress-circular md-mode="indeterminate" class="md-accent"
    ng-show="ctrl.loading" md-diameter="35">
</md-progress-circular>

<md-progress-circular
    md-mode="indeterminate"
    class="md-accent"
    ng-show="ctrl.loading"
    md-diameter="35">
</md-progress-circular>
```

## HTML Quotation Marks

When quoting attributes values, use double quotation marks.

Use double ("") rather than single quotation marks ('') around attribute values.

```html
<!-- Not recommended -->
<a class='maia-button maia-button-secondary'>Sign in</a>

<!-- Recommended -->
<a class="maia-button maia-button-secondary">Sign in</a>
```

## Multimedia Fallback

Provide alternative contents for multimedia.

For multimedia, such as images, videos, animated objects via canvas, make sure to offer alternative access. For images that means use of meaningful alternative text (alt) and for video and audio transcripts and captions, if available.

Providing alternative contents is important for accessibility reasons: A blind user has few cues to tell what an image is about without @alt, and other users may have no way of understanding what video or audio contents are about either.

**(For images whose alt attributes would introduce redundancy, and for images whose purpose is purely decorative which you cannot immediately use CSS for, use no alternative text, as in alt="".)**

```html
<!-- Not recommended -->
<img src="spreadsheet.png">

<!-- Recommended -->
<img src="spreadsheet.png" alt="Spreadsheet screenshot.">
```

## Entity References

Do not use entity references.

There is no need to use entity references like &mdash;, &rdquo;, or &#x263a;, assuming the same encoding (UTF-8) is used for files and editors as well as among teams.

The only exceptions apply to characters with special meaning in HTML (like < and &) as well as control or “invisible” characters (like no-break spaces).

```html
<!-- Not recommended -->
The currency symbol for the Euro is &ldquo;&eur;&rdquo;.

<!-- Recommended -->
The currency symbol for the Euro is “€”.
```

## `type` Attributes

Omit type attributes for style sheets and scripts.

Do not use type attributes for style sheets (unless not using CSS) and scripts (unless not using JavaScript).

Specifying type attributes in these contexts is not necessary as HTML5 implies text/css and text/javascript as defaults. This can be safely done even for older browsers.

```html
<!-- Not recommended -->
<link rel="stylesheet" href="https://www.google.com/css/maia.css"
    type="text/css">

<!-- Recommended -->
<link rel="stylesheet" href="https://www.google.com/css/maia.css">

<!-- Not recommended -->
<script src="https://www.google.com/js/gweb/analytics/autotrack.js"
    type="text/javascript"></script>

<!-- Recommended -->
<script src="https://www.google.com/js/gweb/analytics/autotrack.js"></script>
```

## Semantics

### What are Semantic Elements?

A semantic element clearly describes its meaning to both the browser and the developer.

Examples of non-semantic elements: `<div>` and `<span>` - Tells nothing about its content.

Examples of semantic elements: `<form>`, `<table>`, and `<article>` - Clearly defines its content.

### Use HTML according to its purpose

Use elements (sometimes incorrectly called “tags”) for what they have been created for. For example, use heading elements `<h1>, <h2>, ...` for headings, `<p>` elements for paragraphs, `<a>` elements for anchors, etc.

Using HTML according to its purpose is important for **accessibility**, reuse, and code efficiency reasons.

```html
<!-- Not recommended -->
<div onclick="goToRecommendations();">All recommendations</div>

<!-- Recommended -->
<a href="recommendations/">All recommendations</a>
```

### New Semantic Elements in HTML5

Many web sites contain HTML code like: `<div id="nav"> <div class="header"> <div id="footer">`
to indicate navigation, header, and footer.

HTML5 offers new semantic elements to define different parts of a web page. Here's a list of the most popular with links that provide more details:

| Tag                                                                 | Description                                                                                 |
| ------------------------------------------------------------------- | :------------------------------------------------------------------------------------------ |
| [`<article>`](https://www.w3schools.com/tags/tag_article.asp)       | Defines an article                                                                          |
| [`<aside>`](https://www.w3schools.com/tags/tag_aside.asp)           | Defines content aside from the page content                                                 |
| [`<details>`](https://www.w3schools.com/tags/tag_details.asp)       | Defines additional details that the user can view or hide                                   |
| [`<figcaption>`](https://www.w3schools.com/tags/tag_figcaption.asp) | Defines a caption for a `<figure>` element                                                  |
| [`<figure>`](https://www.w3schools.com/tags/tag_figure.asp)         | Specifies self-contained content, like illustrations, diagrams, photos, code listings, etc. |
| [`<footer>`](https://www.w3schools.com/tags/tag_footer.asp)         | Defines a footer for a document or section                                                  |
| [`<header>`](https://www.w3schools.com/tags/tag_header.asp)         | Specifies a header for a document or section                                                |
| [`<main>`](https://www.w3schools.com/tags/tag_main.asp)             | Specifies the main content of a document                                                    |
| [`<mark>`](https://www.w3schools.com/tags/tag_mark.asp)             | Defines marked/highlighted text                                                             |
| [`<nav>`](https://www.w3schools.com/tags/tag_nav.asp)               | Defines navigation links                                                                    |
| [`<section>`](https://www.w3schools.com/tags/tag_section.asp)       | Defines a section in a document                                                             |
| [`<summary>`](https://www.w3schools.com/tags/tag_summary.asp)       | Defines a visible heading for a `<details>` element                                         |
| [`<time>`](https://www.w3schools.com/tags/tag_time.asp)             | Defines a date/time                                                                         |

**Note**: Please also refer to the [html5 new elements](https://www.w3schools.com/html/html5_new_elements.asp) webpage for more elements .

## Emmet

https://code.visualstudio.com/docs/editor/emmet

## Pug

## Resources

- [Nice HTML & CSS tutorial](https://internetingishard.com/)
- [HTML5 Doctor. Helping you implement HTML5 today](http://html5doctor.com/)

## Sources

We have selected and compiled most of these rules from different resources. The ones that you will find here are the most important to follow. More details can be find in the links below. Feel free to consult these resources at any time.

**Please report broken resources**.

- [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html)
- [HTML5 Style Guide and Coding Conventions](https://www.w3schools.com/html/html5_syntax.asp)
- [Airbnb CSS / Sass Style Guide](https://github.com/airbnb/css)
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- [Markup best practices - 10up](https://10up.github.io/Engineering-Best-Practices/markup/)
- [Github's Primer CSS](https://primer.style/design/global/accessibility)
