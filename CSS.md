# CSS

## Terminology

### Rule declaration

A “rule declaration” is the name given to a selector (or a group of selectors) with an accompanying group of properties. Here's an example:

```css
.listing {
  font-size: 18px;
  line-height: 1.2;
}
```

### Selectors

In a rule declaration, “selectors” are the bits that determine which elements in the DOM tree will be styled by the defined properties. Selectors can match HTML elements, as well as an element's class, ID, or any of its attributes. Here are some examples of selectors:

```css
.my-element-class {
  /* ... */
}

[aria-hidden] {
  /* ... */
}
```

### Properties

Finally, properties are what give the selected elements of a rule declaration their style. Properties are key-value pairs, and a rule declaration can contain one or more property declarations. Property declarations look like this:

```css
/* some selector */
 {
  background: #f1f1f1;
  color: #333;
}
```

## Formatting

- Use soft tabs (2 spaces) for indentation.
- Prefer dashes over camelCasing in class names.
  - Underscores and PascalCasing are okay if you are using BEM (see [OOCSS and BEM](#oocss-and-bem) below).
- Do not use ID selectors.
- When using multiple selectors in a rule declaration, give each selector its own line. Basically, write one selector per line :)
- Write one declaration per line.
- Put a space before the opening brace `{` in rule declarations.
- In properties, put a space after, but not before, the `:` character.
- Put closing braces `}` of rule declarations on a new line.
- Put blank lines between rule declarations.

**Bad**

```css
.avatar {
  border-radius: 50%;
  border: 2px solid white;
}
.no,.nope,.not_good {
  // ...
}
#lol-no {
  // ...
}
// Avoid
.class-1,.class-2,
.class-3 {
  width: 10px;
  height: 20px;
  color: red;
  background-color: blue;
}
```

**Good**

```css
.avatar {
  border-radius: 50%;
  border: 2px solid white;
}

// Prefer
.one,
.selector,
.per-line {
  // ...
}

.class-1,
.class-2,
.class-3 {
  width: 10px;
  height: 20px;
  color: red;
  background-color: blue;
}
```

## Comments

- Prefer line comments (`//` in Sass-land) to block comments.
- Prefer comments on their own line. Avoid end-of-line comments.
- Write detailed comments for code that isn't self-documenting:
  - Uses of z-index
  - Compatibility or browser-specific hacks

## OOCSS and BEM

We encourage some combination of OOCSS and BEM for these reasons:

- It helps create clear, strict relationships between CSS and HTML
- It helps us create reusable, composable components
- It allows for less nesting and lower specificity
- It helps in building scalable stylesheets

**OOCSS**, or “Object Oriented CSS”, is an approach for writing CSS that encourages you to think about your stylesheets as a collection of “objects”: reusable, repeatable snippets that can be used independently throughout a website.

- Nicole Sullivan's [OOCSS wiki](https://github.com/stubbornella/oocss/wiki)
- Smashing Magazine's [Introduction to OOCSS](http://www.smashingmagazine.com/2011/12/12/an-introduction-to-object-oriented-css-oocss/)

**BEM**, or “Block-Element-Modifier”, is a _naming convention_ for classes in HTML and CSS. It was originally developed by Yandex with large codebases and scalability in mind, and can serve as a solid set of guidelines for implementing OOCSS.

- CSS Trick's [BEM 101](https://css-tricks.com/bem-101/)
- Harry Roberts' [introduction to BEM](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)

We recommend a variant of BEM with PascalCased “blocks”, which works particularly well when combined with components (e.g. React). Underscores and dashes are still used for modifiers and children.

**Example**

```jsx
// ListingCard.jsx
function ListingCard() {
  return (
    <article class="ListingCard ListingCard--featured">
      <h1 class="ListingCard__title">Adorable 2BR in the sunny Mission</h1>

      <div class="ListingCard__content">
        <p>Vestibulum id ligula porta felis euismod semper.</p>
      </div>
    </article>
  );
}
```

```css
/* ListingCard.css */
.ListingCard {
}
.ListingCard--featured {
}
.ListingCard__title {
}
.ListingCard__content {
}
```

- `.ListingCard` is the “block” and represents the higher-level component
- `.ListingCard__title` is an “element” and represents a descendant of `.ListingCard` that helps compose the block as a whole.
- `.ListingCard--featured` is a “modifier” and represents a different state or variation on the `.ListingCard` block.

Don't use "grandchild" selectors (i.e. `block__header__title` should probably be `block__title`). All elements are in relation only to their block. [Battling BEM CSS: 10 Common Problems And How To Avoid Them](https://www.smashingmagazine.com/2016/06/battling-bem-extended-edition-common-problems-and-how-to-avoid-them/)

## ID selectors

While it is possible to select elements by ID in CSS, it should generally be considered an anti-pattern. ID selectors introduce an unnecessarily high level of [specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) to your rule declarations, and they are not reusable.

For more on this subject, read [CSS Wizardry's article](http://csswizardry.com/2014/07/hacks-for-dealing-with-specificity/) on dealing with specificity.

## JavaScript hooks

Avoid binding to the same class in both your CSS and JavaScript. Conflating the two often leads to, at a minimum, time wasted during refactoring when a developer must cross-reference each class they are changing, and at its worst, developers being afraid to make changes for fear of breaking functionality.

We recommend creating JavaScript-specific classes to bind to, prefixed with `.js-`:

```html
<button class="btn btn-primary js-request-to-book">Request to Book</button>
```

## Border

Use `0` instead of `none` to specify that a style has no border.

**Bad**

```css
.foo {
  border: none;
}
```

**Good**

```css
.foo {
  border: 0;
}
```

## Font-weight

For the sake of consistency while doing code reviews, we encourage the use of numeric values instead of `bold` or `normal`. Numeric values go from 100 to 900. The keyword value `normal` maps to the numeric value `400` and the value `bold` maps to `700`.

In order to see any effect using values other than `400` or `700`, the font being used must have built-in faces that match those specified weights. Check [font-weight](https://css-tricks.com/almanac/properties/f/font-weight/) article for more great info.

## Line-height

Line-height should be ideally implemented as a multiplier of element's font size. Unitless values: use this number multiplied by the element's font size.

[CSS line-height property
](https://developer.mozilla.org/en-US/docs/Web/CSS/line-height)

[**Example**: Prefer unitless numbers for line-height values](https://developer.mozilla.org/en-US/docs/Web/CSS/line-height#Prefer_unitless_numbers_for_line-height_values)

**Syntax**

```scss
/* Keyword value */
line-height: normal;

/* Unitless values: use this number multiplied
by the element's font size */

line-height: 1.5; /* Preferred */

/* <length> values */
line-height: 3em;

/* <percentage> values */
line-height: 34%;

/* Global values */
line-height: inherit;
line-height: initial;
line-height: unset;
```

### _Accessibility concerns_

Use a minimum value of 1.5 for line-height for main paragraph content. This will help people experiencing low vision conditions, as well as people with cognitive concerns such as Dyslexia. If the page is zoomed to increase the text size, using a unitless value ensures that the line height will scale proportionately.

[W3C Understanding WCAG 2.1](https://www.w3.org/TR/WCAG21/#visual-presentation)

## Shorthand Properties

Use shorthand properties where possible.

CSS offers a variety of shorthand properties (like font) that should be used whenever possible, even in cases where only one value is explicitly set.

Using shorthand properties is useful for code efficiency and understandability.

```scss
/* Not recommended */
border-top-style: none;
font-family: palatino, georgia, serif;
font-size: 100%;
line-height: 1.6;
padding-bottom: 2em;
padding-left: 1em;
padding-right: 1em;
padding-top: 0;

/* Recommended */
border-top: 0;
font: 100%/1.6 palatino, georgia, serif;
padding: 0 1em 2em;
```

If you don’t need to set all the values, don’t use shorthand notation.

```scss
// Avoid

.header-background {
  background: blue;
  margin: 0 0 0 10px;
}

// Prefer

.header-background {
  background-color: blue;
  margin-left: 10px;
}
```

## 0 and Units

Omit unit specification after “0” values, unless required.

Do not use units after 0 values unless they are required.

```scss
flex: 0px; /* This flex-basis component requires a unit. */
flex: 1 1 0px; /* Not ambiguous without the unit, but needed in IE11. */
margin: 0;
padding: 0;
```

## CSS Quotation Marks

Use single ('') rather than double ("") quotation marks for attribute selectors and property values.

Do not use quotation marks in URI values (url()).

Exception: If you do need to use the @charset rule, use double quotation marks—[single quotation marks are not permitted](https://www.w3.org/TR/CSS21/syndata.html#charset).

```scss
/* Not recommended */
@import url('https://www.google.com/css/maia.css');

html {
  font-family: 'open sans', arial, sans-serif;
}

/* Recommended */
@import url(https://www.google.com/css/maia.css);

html {
  font-family: 'open sans', arial, sans-serif;
}
```

## Sass

### Syntax

- Use the `.scss` syntax, never the original `.sass` syntax
- Order your regular CSS and `@include` declarations logically (see below)

### Ordering of property declarations

1. Property declarations

List all standard property declarations, anything that isn't an `@include` or a nested selector.

```scss
.btn-green {
  background: green;
  font-weight: bold;
  // ...
}
```

2. `@include` declarations

Grouping `@include`s at the end makes it easier to read the entire selector.

```scss
.btn-green {
  background: green;
  font-weight: bold;
  @include transition(background 0.5s ease);
  // ...
}
```

3. Nested selectors

Nested selectors, _if necessary_, go last, and nothing goes after them. Add whitespace between your rule declarations and nested selectors, as well as between adjacent nested selectors. Apply the same guidelines as above to your nested selectors.

```scss
.btn {
  background: green;
  font-weight: bold;
  @include transition(background 0.5s ease);

  .icon {
    margin-right: 10px;
  }
}
```

### Variables

Prefer dash-cased variable names (e.g. `$my-variable`) over camelCased or snake_cased variable names. It is acceptable to prefix variable names that are intended to be used only within the same file with an underscore (e.g. `$_my-variable`).

### Mixins

Mixins should be used to DRY up your code, add clarity and consistency, or abstract complexity--in much the same way as well-named functions. Mixins that accept no arguments can be useful for this, but note that if you are not compressing your payload (e.g. gzip), this may contribute to unnecessary code duplication in the resulting styles.

A good example of a mixin that adds consistency is the the `font-size` mixin we use in our projects. We use it to achieve consistency in the size of the font across browsers and devices. Also, expressing font as percentages is good for accessibility purposes ([C12: Using percent for font sizes](https://www.w3.org/TR/2016/NOTE-WCAG20-TECHS-20161007/C12))

```scss
// font size mixin
@mixin font-size($size, $important: 'false') {
  @if $important== 'true' {
    font-size: ($size * 100%/14.5) (!important);
  }

  font-size: ($size * 100%/14.5);
}
```
**_Important_**: For the mixin to work properly and reflect the computed values, you need to setup the html tag and the body tag with specific values.

```scss
html {
  font-size: 62.5%;
}

body {
  font-size: 1.45em!important;
}
```
After setting the previous values, you can use the mixin like this:
```scss
.nice-text {
  // I want my font to be 16px
  @include font-size(16); // This gives you a computed value of 16px
}

.other-text {
  @include font-size(14.5); // This gives you a 100% font size and computed value of 14.5px
}
```

### Extend directive

`@extend` should be avoided because it has unintuitive and potentially dangerous behavior, especially when used with nested selectors. Even extending top-level placeholder selectors can cause problems if the order of selectors ends up changing later (e.g. if they are in other files and the order the files are loaded shifts). Gzipping should handle most of the savings you would have gained by using `@extend`, and you can DRY up your stylesheets nicely with mixins.

### Nested selectors

**Do not nest selectors more than three levels deep!**

```scss
.page-container {
  .content {
    .profile {
      // STOP!
    }
  }
}
```

When selectors become this long, you're likely writing CSS that is:

- Strongly coupled to the HTML (fragile) —OR—
- Overly specific (powerful) —OR—
- Not reusable

Again: **never nest ID selectors!**

If you must use an ID selector in the first place (and you should really try not to), they should never be nested. If you find yourself doing this, you need to revisit your markup, or figure out why such strong specificity is needed. If you are writing well formed HTML and CSS, you should **never** need to do this.

## Performance

CSS performance has been considered not as important as JavaScript performance. However, this doesn’t mean we should ignore it. A sum of small improvements equals better experience for the user.

Three areas of concern are network requests, CSS specificity and animation performance.

Performance best practices are not only for the browser experience, but for code maintenance as well.

### Network Requests

- Limit the number of requests by concatenating CSS files and encoding sprites and font files to the CSS file.
- Minify stylesheets
- Use GZIP compression when possible. most of the times these task are already automated when using build process but always make sure that the code goes minified to production.

### CSS Specificity

Stylesheets should go from less specific rules to highly specific rules. We want our selectors specific enough so that we don’t rely on code order, but not too specific so that they can be easily overridden.

For that purpose, **classes** are our preferred selectors: pretty low specificity but high reusability.

Avoid using `!important` whenever you can.

Use [efficient selectors](https://csswizardry.com/2011/09/writing-efficient-css-selectors/).

```css
// Avoid

div div header#header div ul.nav-menu li a.black-background {
  background: radial-gradient(
    ellipse at center,
    #a90329 0%,
    #8f0222 44%,
    #6d0019 100%
  );
}
```

### Inheritance

Fortunately, many CSS properties can be inherited from the parent. Take advantage of inheritance to avoid bloating your stylesheet but keep specificity in mind.

```css
// Avoid

.sibling-1 {
  font-family: Arial, sans-serif;
}
.sibling-2 {
  font-family: Arial, sans-serif;
}
// Prefer

.parent {
  font-family: Arial, sans-serif;
}
```

### Reusable code

Styles that can be shared, should be shared (aka DRY, Don’t Repeat Yourself). This will make our stylesheets less bloated and prevent the browser from doing the same calculations several times. Make good use of Sass placeholders.

### CSS over assets

Don’t add an extra asset if a design element can be translated in the browser using CSS only. We value graceful degradation over additional HTTP requests.

Very common examples include gradients, triangles, chevrons and any asset that can be represent with CSS. Be creative :) It can be done!

### Animations

It’s a common belief that CSS animations are more performant than JavaScript animations. A few articles aimed to set the record straight (linked below).

If you’re only animating simple state changes and need good mobile support, go for CSS (most cases).
If you need more complex animations, use a JavaScript animation framework or requestAnimationFrame.
Limit your CSS animations to 3D transforms (translate, rotate, scale) and opacity, as those are aided by the GPU and thus smoother. Note that too much reliance on the GPU can also overload it.

```css
// Avoid

#menu li {
  left: 0;
  transition: all 1s ease-in-out;
}
#menu li:hover {
  left: 10px;
}
```

Always test animations on a real mobile device loading real assets, to ensure the limited memory environment doesn’t tank the site.

Articles worth reading:

- [CSS animations performance: the untold story](https://greensock.com/css-performance)
- [Myth Busting: CSS Animations vs. JavaScript](https://css-tricks.com/myth-busting-css-animations-vs-javascript/)
- [CSS vs. JS Animation: Which is Faster?](https://davidwalsh.name/css-js-animation)
- [Why Moving Elements With Translate() Is Better Than Pos:abs Top/left](https://www.paulirish.com/2012/why-moving-elements-with-translate-is-better-than-posabs-topleft/)
- [CSS vs JavaScript Animations](https://developers.google.com/web/fundamentals/look-and-feel/animations/css-vs-javascript?hl=en)
- [requestAnimationFrame](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)

## Responsive websites

TODO:

## SASS Architecture

### Min-width media queries

A responsive website should be built with min-width media queries. This approach means that our media queries are consistent, readable and minimize selector overrides.

- For most selectors, properties will be added at later breakpoints. This way we can reduce the usage of overrides and resets.
- It targets the least capable browsers first which is philosophically in line with mobile first — a concept we often embrace for our sites
- When media queries consistently “point” in the same direction, it makes it easier to understand and maintain stylesheets.

Avoid mixing min-width and max-width media queries.

### Breakpoints

Working with build tools that utilize Sass or PostCSS processing, we can take advantages of reusability and avoid having an unmaintainable number of breakpoints. Using variables and reusable code blocks we can lighten the CSS load and ease maintainability.

### Media queries placement

In your stylesheet files, nest the media query within the component it modifies. **Do not** create size-based partials (e.g. \_1024px.(s)css, \_480px.(s)css): it will be frustrating to hunt for a specific selector through all the files when we have to maintain the project. Putting the media query inside the component will allow developers to immediately see all the different styles applied to an element.

### Folder structure

TODO:

## Resources

- [Nice HTML & CSS tutorial](https://internetingishard.com/)
- [CSS: Just Try and Do a Good Job](https://css-tricks.com/just-try-and-do-a-good-job/)
- [Writing CSS with Accessibility in Mind](https://medium.com/@matuzo/writing-css-with-accessibility-in-mind-8514a0007939)

## Sources

We have selected and compiled most of these rules from different resources. The ones that you will find here are the most important to follow. More details can be find in the links below. Feel free to consult these resources at any time.

**Please report broken resources**.

- [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html)
- [HTML5 Style Guide and Coding Conventions](https://www.w3schools.com/html/html5_syntax.asp)
- [Airbnb CSS / Sass Style Guide](https://github.com/airbnb/css)
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- [Markup best practices - 10up](https://10up.github.io/Engineering-Best-Practices/markup/)
- [Github's Primer CSS](https://primer.style/design/global/accessibility)
